# EmbedMessageCreator

[![Author](https://img.shields.io/badge/Author-Tim54000-brightgreen.svg?style=flat-square)](https://framagit.org/Tim54000) [![Build](https://framagit.org/Tim54000/embedmessagecreator/badges/master/build.svg)]() [![Download](https://img.shields.io/badge/Download-1.1.3-brightgreen.svg?style=flat-square)](//framagit.org/Tim54000/embedmessagecreator/tree/master/builds/1.1.3-RELEASE) [![Needs](https://img.shields.io/badge/Needed-Java 8-orange.svg?style=flat-square)]() [![Dependent](https://img.shields.io/badge/Librairies-JFX 8, Javacord-blue.svg?style=flat-square)]() [![Contributors](https://img.shields.io/badge/Go to-Contributors-lightgrey.svg?style=flat-square)](https://framagit.org/Tim54000/embedmessagecreator/graphs/master)

**Breaks the Wall** with Embed message in Discord.

## Why use EmbedMessageCreator?

Using EmbedMessageCreator you can **simplify your life** and thus write in an original way compared to your friends. EmbedMessageCreator makes it possible **to use all the know-how of Embed of Discord using only a GUI.**

## Quickstart

To use EmbedMessageCreator it is enough to have **Java 8**.

Then just enter the information requested in the corresponding fields.

> **Warning**: The fields "server id", "channel name" and "token" are required, otherwise the message will not be sent.

## Downloads

**The current version is :** 1.1.3.
To download EmbedMessageCreator, please visit [here](https://framagit.org/Tim54000/embedmessagecreator/tree/master/builds/).

## Contributing

EmbedMessageCreator is a personal Open-Source project by [Tim54000](https://framagit.org/Tim54000).
Contributions are welcomed. You may too :

 - [Subscribe on FramaGit](https://framagit.org/Tim54000)
 - Spread EmbedMessageCreator to the world!
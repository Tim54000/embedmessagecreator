package fr.tim54000.discord.EmbedMessageCreator;

import com.google.common.util.concurrent.FutureCallback;
import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.User;
import de.btobastian.javacord.entities.message.embed.EmbedBuilder;
import javafx.stage.Stage;

import java.awt.*;
import java.util.Scanner;

import static fr.tim54000.discord.EmbedMessageCreator.Main.sendlog;


/**
 * Created by Adélie on 08/05/2017.
 */
public class DiscordManager {
    private static boolean stoped = true;
    private static boolean tokenfail =false;
    private static DiscordAPI api;
    private Main main;


    /**
     *  Start the bot for send a message
     * @param token User's Token / Bot's Token
     * @param bot if the token is for a bot
     */
    public void initApp(String token, boolean bot) {
        if (token == null || token == "" || token == " ") {
            System.out.println("The token is invalid !");
            Stop();
        }
        init(token, bot);
        while (stoped && !tokenfail) {
            sendlog("Attempt to connect !");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Init the DiscordAPi of Javacord
     * @param arg Token
     * @param bot if the token is for a bot
     * @return true if the connection is Okay
     */
    private boolean init(String arg, boolean bot) {
        DiscordAPI iapi = Javacord.getApi(arg, bot);
        iapi.connect(new FutureCallback<DiscordAPI>() {
            @Override
            public void onSuccess(DiscordAPI iapi) {
                // register listener
                stoped = false;
                api = iapi;
                sendlog("Started !");

            }

            @Override
            public void onFailure(Throwable t) {


                tokenfail = true;
                Stop();
                t.printStackTrace();

            }
        });
        return stoped;
    }


    /**
     * Stop the API
     */
    public void Stop() {
        if (!stoped) {
            api.disconnect();

        }
        stoped = true;
        sendlog("Arret du bot !");


        //System.exit(0);

    }

    /**
     *
     * @return Discord APi of connection
     */
    public DiscordAPI getApi() {
        return api;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public EmbedBuilder getEmbed(String author, String msg) {
        User Uauthor = null;
        for (User u : api.getUsers()) {
            if (u.getName().toLowerCase().contains(author.toLowerCase())) {
                Uauthor = u;
            }
        }

        if (Uauthor != null) {
            EmbedBuilder eb = new EmbedBuilder().setAuthor(author, "", Uauthor.getAvatarUrl().toString())
                    .setColor(Color.cyan)
                    .setDescription(msg);
            return eb;
        } else {
            EmbedBuilder eb = new EmbedBuilder().setAuthor(author)
                    .setColor(Color.cyan)
                    .setDescription(msg);
            return eb;
        }


    }
    public boolean isStoped(){
        return stoped;
    }
    public boolean isFail(){
        return tokenfail;
    }
}

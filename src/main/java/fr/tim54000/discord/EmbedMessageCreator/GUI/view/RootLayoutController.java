package fr.tim54000.discord.EmbedMessageCreator.GUI.view;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.entities.Channel;
import de.btobastian.javacord.entities.Server;
import de.btobastian.javacord.entities.User;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.entities.message.embed.EmbedBuilder;
import fr.tim54000.discord.EmbedMessageCreator.DiscordManager;
import fr.tim54000.discord.EmbedMessageCreator.FieldEmbed;
import fr.tim54000.discord.EmbedMessageCreator.Main;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Adélie on 08/05/2017.
 */
public class RootLayoutController {
    private Main mainApp;

    @FXML
    private TextField tokenField;

    @FXML
    private CheckBox botField;

    @FXML
    private TextField serverIDField;

    @FXML
    private TextField channelField;

    @FXML
    private TextField embedTitleField;

    @FXML
    private TextField embedAuthorNameField;

    @FXML
    private TextField embedAuthorURLField;

    @FXML
    private TextField embedAvatarURLField;

    @FXML
    private ColorPicker embedColorField;

    @FXML
    private TextField embedThumbnailURLField;

    @FXML
    private TextField embedPictureURLField;

    @FXML
    private TextField embedFooterField;

    @FXML
    private TextField mentionsField;

    @FXML
    private TextField embedURLField;

    @FXML
    private TextArea embedDescriptionField;

    @FXML
    private TextArea textField;

    @FXML
    private TableView FieldTab;

    @FXML
    private javafx.scene.control.TableColumn<FieldEmbed, String> nameTab;

    @FXML
    private TableColumn<FieldEmbed, String> valueTab;

    @FXML
    private TableColumn<FieldEmbed, String> inlineTab;

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
       FieldTab.setItems(mainApp.getField());
    }

    /**
     * On click on the reset button
     */
    @FXML
    private void handleReset() {
       /* tokenField.clear();
        serverIDField.clear();
        channelField.clear();*/
       embedAuthorNameField.clear();
       embedAuthorURLField.clear();
       embedAvatarURLField.clear();
       embedColorField.setValue(Color.AZURE);
       embedDescriptionField.clear();
       embedFooterField.clear();
       embedPictureURLField.clear();
       embedThumbnailURLField.clear();
       embedTitleField.clear();
       textField.clear();
       embedURLField.clear();
       mentionsField.clear();
       FieldTab.getItems().clear();


    }


    /**
     * Layout's start
     */
    @FXML
    public void initialize(){
        nameTab.setCellValueFactory(
                cellData -> cellData.getValue().getName());
        valueTab.setCellValueFactory(
                cellData -> cellData.getValue().getValue());
        inlineTab.setCellValueFactory(
                cellData -> cellData.getValue().getInline());
    }

    /**
     * On click on the addField button
     */
    @FXML
    private void handleAddField(){
        FieldEmbed fe = new FieldEmbed();
        boolean isOk =mainApp.showAddFieldDialog(fe);
        if(isOk){
            mainApp.getField().add(fe);
        }
    }

    /**
     * On click on the removeField button
     */
    @FXML
    private void handleRemoveField(){
        Object o = FieldTab.getSelectionModel().getSelectedItem();
        FieldTab.getItems().remove(o);
    }

    /**
     * On click on the send button
     */
    @FXML
    private void handleSend() throws ExecutionException, InterruptedException {
        DiscordManager dm = new DiscordManager();
        dm.initApp(tokenField.getText(), botField.isSelected());

        DiscordAPI api = dm.getApi();
        /*for (Server s : api.getServers()) {
            for (Channel ch : s.getChannels()) {
                if (ch.getName().equalsIgnoreCase("idees")) {
                    EmbedBuilder eb = dm.getEmbedIdea(pseudoField.getText(), propositionField.getText());

                    Future<Message> msgs = ch.sendMessage("", eb);



                }
            }
        }*/
        if (!dm.isFail()) {

            if (api.getServerById(serverIDField.getText()) != null) {
                Server server = api.getServerById(serverIDField.getText());
                Channel channel = null;
                for (Channel ch : server.getChannels()) {
                    if (ch.getName().toLowerCase().contains(channelField.getText().toLowerCase())) {
                        channel = ch;
                        break;
                    }
                }

                if (channel != null) {


                    sendMessage(channel, dm);


                    Main.sendlog("Message sent !");
                } else {
                    channelNameInvalid(channelField.getText(), server);
                }
            } else {
                serverInvalid(serverIDField.getText());
            }

            dm.Stop();
            dm = null;

        }
        else{
            tokenInvalid();
        }
    }

    /**
     * Send a Discord message
     * @param channel Where would you like post the message
     * @param dm The DiscordManager
     */
    private void sendMessage(Channel channel, DiscordManager dm) {
        DiscordAPI api = dm.getApi();
        EmbedBuilder eb = dm.getEmbed(embedAuthorNameField.getText(), embedDescriptionField.getText());
        if(!(embedAvatarURLField.getText().equalsIgnoreCase("") || embedAuthorURLField.getText().equalsIgnoreCase(""))){
            eb.setAuthor(embedAuthorNameField.getText(), embedAuthorURLField.getText(), embedAvatarURLField.getText());

        }
        float r,g,b = 0;

        r= (float) embedColorField.getValue().getRed();
        g = (float) embedColorField.getValue().getGreen();
        b = (float) embedColorField.getValue().getBlue();
        eb.setColor(new java.awt.Color(r,g,b));


        eb.setTitle(embedTitleField.getText());
        eb.setFooter(embedFooterField.getText());
        eb.setImage(embedPictureURLField.getText());
        eb.setThumbnail(embedThumbnailURLField.getText());
        eb.setUrl(embedURLField.getText());

        for(FieldEmbed fe : mainApp.getField()){
            eb.addField(fe.getName().getValue(),fe.getValue().getValue(),true);
        }

        StringBuilder sb = new StringBuilder();
        for(User u : getUsersbyName(channel,mentionsField.getText().split(" "))){
            sb.append(u.getMentionTag()+" ");
        }


        Future<Message> msg = channel.sendMessage(sb.toString()+textField.getText(), eb);
        while(!msg.isDone()){
            Main.sendlog("Attempt to send the message !");
        }

    }


    private ArrayList<User> getUsersbyName(Channel channel, String[] usernameslist) {
        ArrayList<String> usernames = new ArrayList<>();
        ArrayList<User> users = new ArrayList<>();
        for(String s : usernameslist){
            usernames.add(s.toLowerCase());
        }
        for(User u : channel.getServer().getMembers()){
            if(usernames.contains(u.getName().toLowerCase())){
                users.add(u);
            }
        }
        return users;
    }

    public void channelNameInvalid(String name, Server s){
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.initOwner(Main.primaryStage);
        a.setTitle("Error : Channel is invalid !");
        a.setHeaderText("The channel name is invalid...");
        a.setContentText("The channel `"+name+"` doesn't exist in the "+s.getName()+" server !");
    }

    public void tokenInvalid(){
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.initOwner(Main.primaryStage);
        a.setTitle("Error : Token is invalid !");
        a.setHeaderText("The Token is invalid !");
        a.setContentText("Your token is wrong...");
    }
    public void serverInvalid(String serverid){
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.initOwner(Main.primaryStage);
        a.setTitle("Error : Server ID is invalid !");
        a.setHeaderText("The Server ID is invalid !");
        a.setContentText("The server with id `"+serverid+"` doesn't exist !");
    }




}

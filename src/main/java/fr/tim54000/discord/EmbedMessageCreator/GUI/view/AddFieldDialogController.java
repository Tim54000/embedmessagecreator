package fr.tim54000.discord.EmbedMessageCreator.GUI.view;

import fr.tim54000.discord.EmbedMessageCreator.FieldEmbed;
import fr.tim54000.discord.EmbedMessageCreator.Main;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Created by Adélie on 14/05/2017.
 */
public class AddFieldDialogController {

    private Stage stage;
    private FieldEmbed fe;

    private boolean isOk = false;
    private Main m;

    @FXML
    private TextField nameField;

    @FXML
    private TextField valueField;

    @FXML
    private CheckBox inlineField;

    public void setDialogStage(Stage dialogStage) {
        stage = dialogStage;
    }


   public void setMainApp(Main mainApp){
        m = mainApp;
   }

    /**
     * On click on OK button in the dialog
     */
    @FXML
    private void handleOK(){
       if(isValid()) {
           fe.setName(nameField.getText());
           fe.setValue(valueField.getText());
           fe.setInline(inlineField.isSelected());
           isOk = true;
           stage.close();
       }else{
           Main.sendlog("is invalid !");
       }
    }

    /**
     *
     * @return true if fields are corrects !
     */
    private boolean isValid() {
        if(nameField.getText() != null && valueField.getText() != null){
            if(nameField.getText() != "" &&valueField.getText() != ""){
                if (valueField.getText() != " " &&nameField.getText() != " "){
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * On click on cancel button in the dialog
     */
    @FXML
    private void handleCancel(){
        isOk = false;
        stage.close();
    }

    public void setFieldEmbed(FieldEmbed fe) {
        this.fe = fe;
    }


    /**
     *
     * @return true if user has clicked on Ok button and if fields are corrects
     */
    public boolean isOkClicked() {
        return isOk;
    }
}

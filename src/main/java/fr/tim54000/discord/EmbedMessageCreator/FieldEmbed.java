package fr.tim54000.discord.EmbedMessageCreator;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Adélie on 16/05/2017.
 */
public class FieldEmbed {

    private final StringProperty name;
    private final StringProperty value;
    private final BooleanProperty inline;

    public FieldEmbed() {
        this(null, null, false);
    }


    public FieldEmbed(String Name, String Value, boolean inline) {
    name = new SimpleStringProperty(Name);
    value = new SimpleStringProperty(Value);
    this.inline = new SimpleBooleanProperty(inline);
    }


    public StringProperty getName() {
        return name;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty getValue() {
        return value;
    }

    public StringProperty valueProperty() {
        return value;
    }

    public void setValue(String value) {
        this.value.set(value);
    }

    public StringProperty getInline() {

        if(inline.getValue()){
            return new SimpleStringProperty("true");
        }else
        {
            return new SimpleStringProperty("false");
        }
    }
    public void setInline(boolean inline) {
        this.inline.set(inline);
    }

}

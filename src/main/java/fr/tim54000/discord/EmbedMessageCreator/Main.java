package fr.tim54000.discord.EmbedMessageCreator;

import com.diogonunes.jcdp.color.ColoredPrinter;
import fr.tim54000.discord.EmbedMessageCreator.GUI.view.AddFieldDialogController;
import fr.tim54000.discord.EmbedMessageCreator.GUI.view.RootLayoutController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Adélie on 08/05/2017.
 */
public class Main extends Application {

    private static ColoredPrinter cp = new ColoredPrinter.Builder(0, false).build();
    private static String token = "MzA4NjI4MjcxMjc1MTgwMDMz.C-jp5A.JIZ6EtArIEfg09w_tpmncc_j_l8";
    public static Stage primaryStage = null;
    private static BorderPane rootLayout;
    private ObservableList<FieldEmbed> field =  FXCollections.observableArrayList();

    /**
     * Method to send a message in the console without color.
     *
     * @param o An object to send in the console
     */
    public static void sendlog(Object o) {
        cp.clear();
        cp.println(o);
        cp.clear();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        sendlog("Chargement de la fenetre!");
        this.primaryStage.setTitle("EmbedMessageCreator");
        this.primaryStage.getIcons().add(new Image(this.getClass().getResourceAsStream("/imgs/icon.png")));
        this.primaryStage.setResizable(true);
        this.primaryStage.setMaxWidth(640);
        this.primaryStage.setMaximized(false);
        initRootLayout();
    }


    /**
     * Init the first layout of GUI
     */
    public void initRootLayout() {
        sendlog("Initialisation !");
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class
                    .getResource("/view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     *  Show a dialog for set a new FieldEmbed
     *
     * @param fe FieldEmbed to set by the user
     * @return true if the dialog is set
     */
    public boolean showAddFieldDialog(FieldEmbed fe) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/view/AddFieldDialog.fxml"));
            GridPane page = (GridPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Add Field !");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            AddFieldDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setFieldEmbed(fe);
            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();


            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ObservableList<FieldEmbed> getField() {
        return field;
    }
}

# Changelog

## Versions 1.1.3 :
+ Compatibility with small resolutions added !
+ Adding CSS!

## Versions 1.1.2.1 :
+ JavaDoc added !

## Versions 1.1.2 :
+ Embed Fields added !

## Versions 1.0.0 :
+ Basic Fields added !
+ Mentions Fields added !
